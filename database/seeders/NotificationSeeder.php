<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notifications')->insert([
            [
                'notification_content' => 'hallo selamat datang',
                'notification_time' => '2 jam yang lalu'

            ],
            [
                'notification_content' => 'hallo selamat datang Fatima',
                'notification_time' => '1 jam yang lalu'
            ]

        ]);
    }
}
