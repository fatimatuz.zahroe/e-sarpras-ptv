{{-- Extends layout --}}
@extends('adm.layout.default')

{{-- Styles --}}
@section('styles')
    
@endsection

{{-- Content --}}
@section('content')
    @include('adm.home.dump_content')
@endsection

{{-- Script --}}
@section('scripts')
    
@endsection