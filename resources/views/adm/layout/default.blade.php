<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta9
* @link https://tabler.io
* Copyright 2018-2022 The Tabler Authors
* Copyright 2018-2022 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
        <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
        <title>Dashboard - Tabler - Premium and Open Source dashboard template with responsive and high quality UI.</title>
        
        {{-- Default CSS --}}
        <link href="{{ asset('adm/dist/css/tabler.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('adm/dist/css/tabler-flags.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('adm/dist/css/tabler-payments.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('adm/dist/css/tabler-vendors.min.css') }}" rel="stylesheet"/>
        <link href="{{ asset('adm/dist/css/demo.min.css') }}" rel="stylesheet"/>

        {{-- Includable CSS --}}
		@yield('styles')

    </head>
    <body>
        <div class="page">

            {{-- Navbar --}}
            @include('adm.layout.navbar')

            {{-- Content --}}
            @yield('content')
            
        </div>
        
        <!-- Libs JS -->
        <script src="{{ asset('adm/dist/libs/apexcharts/dist/apexcharts.min.js') }}"></script>
        <script src="{{ asset('adm/dist/libs/jsvectormap/dist/js/jsvectormap.min.js') }}"></script>
        <script src="{{ asset('adm/dist/libs/jsvectormap/dist/maps/world.js') }}"></script>
        <script src="{{ asset('adm/dist/libs/jsvectormap/dist/maps/world-merc.js') }}"></script>
        <!-- Tabler Core -->
        <script src="{{ asset('adm/dist/js/tabler.min.js') }}"></script>
        <script src="{{ asset('adm/dist/js/demo.min.js') }}"></script>

        {{-- Includable JS --}}
        @yield('scripts')

    </body>
</html>