<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// PUBLIC
Route::get('/', [App\Http\Controllers\pbl\HomeController::class, 'index'])->name('pbl.home');
Route::view('/signin', 'adm.login.base')->name('signin');

Route::get('/admin', [\App\Http\Controllers\adm\HomeController::class,'index'])->name('adm.home');

// Route::resource('/profile-pt/kelembagaan', [\App\Http\Controllers\adm\profile-pt\KelembagaanController::class,'index'])->name('profile-pt.kelembagaan');